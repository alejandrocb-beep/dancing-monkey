import { Component, ViewChild } from '@angular/core';
import { AlertController, IonRange, NavController, Platform, ToastController } from '@ionic/angular';
import { Howl } from 'howler';
import { Plugins, FilesystemDirectory, FilesystemEncoding, ReaddirResult } from '@capacitor/core';

export interface Track {
  name: string;
  path: string;
  album: string;
  grup: string;
  id: number;
}


const { Filesystem } = Plugins;


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  pr = this.readdir();

  activeTrack: Track = null;
  player: Howl = null;
  isPlaying = false;
  progress = 0;
  azul: any;
  @ViewChild('range', {static: false }) range: IonRange;

  constructor() {  
    this.readdir();
  }


  // leer directorios
  async readdir() {
    try {
      let ret = await Filesystem.readdir({
        path: ''
      });
      this.azul = ret.files;
    } catch(e) {
      console.error('Unable to read dir', e);
    }
  }

  // intentar

  //LAS COSAS DEL MP3

  start(track: Track){
    
    if (this.player){
      this.player.stop();
    }
    this.player = new Howl({
      src: [track.path],
      html5: true,
      onplay: () => {
        console.log('al clicar');
        this.isPlaying = true;
        this.activeTrack = track;
        this.updateProgress();
      },
      onend: () => {
        console.log('al acabar');
        this.next();
      }
    })
    this.player.play();
  }

  togglePlayer(pause){
    this.isPlaying = !pause;
    if (pause){
      this.player.pause();
    } else {
      this.player.play();
    }
  }

  next(){
    let index = this.playlist.indexOf(this.activeTrack);
    if (index != this.playlist.length -1){
      this.start(this.playlist[index + 1]);
    } else {
      this.start(this.playlist[0]);
    }
  }

  prev(){
    let index = this.playlist.indexOf(this.activeTrack);
    if (index > 0){
      this.start(this.playlist[index - 1]);
    } else {
      this.start(this.playlist[this.playlist.length - 1]);
    }
  }

  seek(){
    let newValue =+ this.range.value;
    let duration = this.player.duration();
    this.player.seek(duration * (newValue / 100));
  }

  updateProgress(){
    let seek = this.player.seek();
    this.progress = (seek / this.player.duration()) * 100 || 0;
    setTimeout(() => {
      this.updateProgress();
    }, 100)
  }


  playlist: Track[] = [
    {
      name: 'DriverInTheBack: Ones Again',
      path: './assets/Musica/onesagain.mp3',
      album: 'theoralcigarettes',
      grup: 'driverintheback',
      id: 1
    },
    {
      name: 'DriverInTheBack: Dip-Bap',
      path: './assets/Musica/dib-bap.mp3',
      album: 'theoralcigarettes',
      grup: 'driverintheback',
      id: 2
    },
    {
      name: 'DriverInTheBack: Kirai',
      path: './assets/Musica/kirai.mp3',
      album: 'theoralcigarettes',
      grup: 'driverintheback',
      id: 3
    },
    {
      name: 'DriverInTheBack: Kishi Kaisei Story',
      path: './assets/Musica/kishikaiseistory.mp3',
      album: 'theoralcigarettes',
      grup: 'driverintheback',
      id: 4
    },
    {
      name: 'DriverInTheBack: Kntanna Kotto',
      path: './assets/Musica/kntannakoto.mp3',
      album: 'theoralcigarettes',
      grup: 'driverintheback',
      id: 5
    },
    {
      name: 'DriverInTheBack: Sesshoku',
      path: './assets/Musica/sesshoku.mp3',
      album: 'theoralcigarettes',
      grup: 'driverintheback',
      id: 6
    },
    {
      name: 'DriverInTheBack: Uh...man',
      path: './assets/Musica/uh...man.mp3',
      album: 'theoralcigarettes',
      grup: 'driverintheback',
      id: 7
    },
    {
      name: 'Artic Monkeys: Mad Sounds',
      path: './assets/Musica/madsounds.mp3',
      album: 'am',
      grup: 'articmonkeys',
      id: 8
    },
    {
      name: 'Artic Monkeys: No.1 Party Anthem',
      path: './assets/Musica/no1party.mp3',
      album: 'am',
      grup: 'articmonkeys',
      id: 9
    },
    {
      name: 'Artic Monkeys: One for the Road',
      path: './assets/Musica/onefr.mp3',
      album: 'am',
      grup: 'articmonkeys',
      id: 10
    },
    {
      name: 'Artic Monkeys: R U Mine',
      path: './assets/Musica/rumine.mp3',
      album: 'am',
      grup: 'articmonkeys',
      id: 11
    },
    {
      name: 'Artic Monkeys: Snap Out of it',
      path: './assets/Musica/sofi.mp3',
      album: 'am',
      grup: 'articmonkeys',
      id: 12
    }
  ];


}
