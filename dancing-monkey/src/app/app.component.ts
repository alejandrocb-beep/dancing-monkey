import { Component } from '@angular/core';
import { File, Entry } from '@ionic-native/file/ngx';
import { Platform } from '@ionic/angular';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  navigate : any;
  constructor(private platform    : Platform) 
  {
    this.sideMenu();
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
    });
  }

  sideMenu()
  {
    this.navigate =
    [
      {
        title : "Canciones",
        url   : "/home",
        icon  : "home"
      },
      {
        title : "Playlists",
        url   : "/playlists",
        icon  : "chatboxes"
      },
    ]
  }
}